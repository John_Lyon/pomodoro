import React from 'react';


export default class TodosListHeader extends React.Component {
    render() {
        return (
            <thead>
                <tr className="bg-info">
                    <th className="text-center">Task</th>
                    <th className="text-center">Action</th>
                    <th className="text-center">Pomodoros</th>
                    <th className="text-center">Start Task</th>
                </tr>
            </thead>
        );
    }
}