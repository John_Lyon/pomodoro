import React from 'react';
import CreateTodo from './create-todo';
import TodosList from './todos-list';

// const todos = [];
const todos = [
    {
        task: '#MakeYoutubeGreatAgain',
        isCompleted: false,
        pomodoro: 1,
        disabled: true
    }, 
    {
        task: '#MakeAmericaGreatAgain',
        isCompleted: true,
        pomodoro: 1,
        disabled: false
    },
    {
        task: 'trololo',
        isCompleted: true,
        pomodoro: 2,
        disabled: false
    }, 
    {
        task: 'Add Pomodoro',
        isCompleted: false,
        pomodoro: 3,
        disabled: false
    }
];

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state= {
            todos: todos
        };

        Notification.requestPermission();
    }

    render() {
        return (
            <div>
                <TodosList 
                    todos={this.state.todos}
                    toggleTask={this.toggleTask.bind(this)}
                    saveTask={this.saveTask.bind(this)}
                    deleteTask={this.deleteTask.bind(this)}
                />
                <CreateTodo 
                    todos={this.state.todos} 
                    createTask={this.createTask.bind(this)} 
                />
            </div>
        );
    }


    toggleTask(task) {
        const foundTodo = _.find(this.state.todos, todo => todo.task === task);

        foundTodo.isCompleted = !foundTodo.isCompleted;

        this.setState({
            todos: this.state.todos 
        });
    }

    createTask(task, pomodoro) {
        this.state.todos.push({
            task: task,
            isCompleted: false,
            pomodoro: pomodoro
        });

        this.setState({ todos: this.state.todos});
    }

    saveTask(oldTask, newTask, oldPomodoro, newPomodoro) {
        const foundTodo = _.find(this.state.todos, todo => todo.task === oldTask);
        
        foundTodo.task = newTask;
        foundTodo.pomodoro = newPomodoro;

        this.setState({
            todos: this.state.todos
        });
    }

    deleteTask(taskToDelete) {

        _.remove(this.state.todos, todo => todo.task === taskToDelete);

        this.setState({
            todos: this.state.todos
        });
    }
}