import React from 'react';
import Timer from './timer';
import Modal from 'react-modal';



export default class TodosListItem extends React.Component {
    constructor(props) {

        super(props);

        this.state = {
            isEditing: false,
            showModal: false
        };

        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);

    }

    handleOpenModal () {
        this.setState({ showModal: true });
    }
      
    handleCloseModal () {
        this.setState({ showModal: false });

    }

    renderTaskSection() {
        const { task, isCompleted, pomodoro } = this.props;


        const taskStyle = {
            textDecoration: isCompleted ? 'line-through' : 'none',
            cursor: 'pointer'
        };

        if(this.state.isEditing) {
            return (
                <td>
                    <form className="form-inline" onSubmit={this.onSaveClick.bind(this)}>
                        <div className="form-group">
                            <input className="form-control" type="text" maxLength="80" defaultValue={task} ref="editInput" />
                            <input className="form-control" type="number" min="1" max="12" defaultValue={pomodoro} ref="editPomoInput" />
                        </div>
                    </form>
                </td>
            );
        }

        return (
            <td style={taskStyle}
                onClick={this.props.toggleTask.bind(this, task)}>
                {this.props.task}
            </td>
        );
    }

    renderActionSection() {
        if (this.state.isEditing) {
            return (
                <td>
                    <button className="btn btn-default btn-success" onClick={this.onSaveClick.bind(this)}>Save</button>
                    <button className="btn btn-default btn-danger" onClick={this.onCancelClick.bind(this)}>Cancel</button>
                </td>
            );
        }

        return (
            <td>
                <button className="btn btn-default btn-info" onClick={this.onEditClick.bind(this)}>Edit</button>
                <button className="btn btn-default btn-danger" onClick={this.props.deleteTask.bind(this, this.props.task)}>Delete</button>
            </td>
        );
    }

    // pomodoro display
    renderPomodoroSection() {
        let pomodoroCount = [];
        for(var i = 1; i <= this.props.pomodoro; i++) {
            pomodoroCount.push(<span key={i} className="pomodoro-img"></span>);
            if (i == 6) {
                pomodoroCount.push(<br key={'br_' + i}/>);
            }
        }
        return (
            <td>
                {pomodoroCount}
            </td>
        );
    }

    renderTimeModalTrigger() {
        return (
            <td>
                <button className="btn btn-default btn-success" onClick={this.handleOpenModal}>Start</button>
                <Modal
                  isOpen={this.state.showModal}
                  contentLabel="Example Modal"
                >
                    <Timer 
                        isCounting={false}
                        pomodoroCount={this.props.pomodoro}
                        closeModal={this.handleCloseModal.bind(this)}
                        isWorking={'working'}
                        deleteTask={this.props.deleteTask.bind(this, this.props.task)}
                    />
                    <div className="text-center">
                        <h2>{this.props.task}</h2>
                        <button className="btn" onClick={this.handleCloseModal}>Cancel</button>
                    </div>
                </Modal>
            </td>
        );
    }

    render() {

        return (
            <tr>
                {this.renderTaskSection()}
                {this.renderActionSection()}
                {this.renderPomodoroSection()}
                {this.renderTimeModalTrigger()}
            </tr>
        );
    }

    onEditClick() {
        this.setState({
            isEditing: true
        });
    }

    onCancelClick() {
        this.setState({ 
            isEditing: false
        });
    }


    onSaveClick(event) {
        event.preventDefault();

        const oldTask = this.props.task;
        const newTask = this.refs.editInput.value;

        const oldPomodoro = this.props.pomodoros;
        const newPomodoro = this.refs.editPomoInput.value;

        this.props.saveTask(oldTask, newTask, oldPomodoro, newPomodoro);
        this.setState({ isEditing: false });
    }
}