import React from 'react';

const workTime = 25 * 1000 * 60;
const breakTime = 5 * 1000 * 60;


export default class Timer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            date: workTime ,
            isCounting: this.props.isCounting,
            isWorking: this.props.isWorking
        };

        this.pomodoroCount = this.props.pomodoroCount;

    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.time(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    time() {
        if (this.state.isCounting) {
            this.setState({
                date: this.state.date - 1000
            });
        }
    }

    timeHandler() {
        const time = this.state.date;

        const seconds = Math.floor(time / 1000) % 60;
        const minutes = Math.floor(time / 1000 / 60) % 60;
        const hours = Math.floor(time / 1000 / 60 / 60) % 24;

        let result = (hours ? hours + ':': '') + (minutes < 10 ? '0' : '') + minutes + ':' + (seconds < 10 ? '0' : '') + seconds;

        if (time <= 0 ) {
            this.zeroCount();
        }

        return result;
    }

    zeroCount() {

        let notify1;

        if (this.state.isWorking == 'working') {

            this.state = {
                date: breakTime,
                isWorking: 'resting'
            };

            notify1 = new Notification('Take a break');

        } else if (this.state.isWorking == 'resting' && this.pomodoroCount == 1) {

            this.state = {
                isWorking: 'finished'
            };

            notify1 = new Notification('No more pomodoros for this one, chose another task');

        } else if (this.state.isWorking == 'resting') {

            this.state = {
                date: workTime,
                isWorking: 'working'
            };

            this.pomodoroCount = this.pomodoroCount - 1;
            notify1 = new Notification('Get back to work, you have ' + this.pomodoroCount + ' pomodoro(s) to go');

        }

        notify1.onclick = function () {
            window.focus();
        };
    }


    renderTimerButton() {
        if (this.state.isWorking == 'finished') {
            return (
                <button className="btn" onClick={this.onFinishClick.bind(this)}>Start new task</button>
            );
        } else if (!this.state.isCounting) {
            return (
                <button className="btn" onClick={this.onStartClick.bind(this)}><i className="glyphicon glyphicon-play"></i></button>
            );
        } else {
            return (
                <button className="btn" onClick={this.onStopClick.bind(this)}><i className="glyphicon glyphicon-pause"></i></button>
            );
        }
        
    }

    render() {
        return (
            <div className="text-center">
                <h1 className="time">{this.timeHandler()}</h1>
                {this.state.isWorking}<br/>
                {this.renderTimerButton()}
            </div>
        );
    }

    onStartClick() {
        this.setState({
            isCounting: true
        });
    }

    onFinishClick() {
        this.props.closeModal();
        this.props.deleteTask();
    }

    onStopClick() {
        this.setState({
            isCounting: false
        });
    }
}