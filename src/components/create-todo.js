import React from 'react';

export default class CreateTodo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null
        };
    }

    renderError() {
        if (!this.state.error) {
            return null;
        }

        return <div style={{color:'red'}}>{this.state.error}</div> ;
    }

    render() {
        return (
                <form className="form-horizontal text-center" onSubmit={this.handleCreate.bind(this)}>
                    <div className="form-group ">
                        <div className="col-xs-10 col-sm-5 col-sm-offset-2">
                            <input className="form-control" type="text" maxLength="80" placeholder="Enter task" ref="createInput" />
                        </div>
                        <div className="col-xs-2 col-sm-1">
                            <input className="form-control" type="number" min="1" max="12" ref="createPomodoro" defaultValue="1"/>
                        </div>
                        <div className="col-xs-12 col-sm-2">
                            <button className="btn btn-block create">Create</button>
                        </div>
                        {this.renderError()}
                    </div>
                </form>
        );
    }

    handleCreate(event) {
        event.preventDefault();

        const createInput = this.refs.createInput;
        const task = createInput.value;
        const validateInput = this.validateInput(task);


        if (validateInput) {
            this.setState({ error: validateInput });
            return ;
        }

        this.setState({error: null});
        this.props.createTask(this.refs.createInput.value, this.refs.createPomodoro.value);
        this.refs.createInput.value = '';
        this.refs.createPomodoro.value = 1;
    }

    validateInput(task) {
        if (!task) {
            return 'Please enter a task';
        } else if (_.find(this.props.todos, todo => todo.task === task)) {
            return 'Task already exists';
        } else {
            return null;
        }
    }
}